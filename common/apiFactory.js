app.factory("apiFactory", function ($http, $q, APIMETHODTYPE, CONFIG, $location) {

    return {
        invokeApi: function (url, apiMethodType, apiData, isFileUpload) {

            return this.invokeApiUsingHttp(CONFIG.API_BASEURL + url, apiMethodType, apiData, isFileUpload);
        },
        invokeApiUsingHttp: function (url, apiType, apiData, isFileUpload) {
            var deferred = $q.defer();
            switch (apiType) {
                case APIMETHODTYPE.POST:
                    if (typeof isFileUpload != 'undefined') {
                        $http.post(url, apiData, {
                            transformRequest: angular.identity,
                            headers: { 'Content-Type': undefined }
                        })
                           .then(function (response) {
                               deferred.resolve(response.data);
                           },
                           function (response) {
                               deferred.resolve(response.data);
                           });
                    } else {
                        $http.post(url, apiData)
		 				.then(function (response) {
		 				    deferred.resolve(response.data);
		 				},
		 				function (response) {
		 				    deferred.resolve(response.data);
		 				});
                    }
                    return deferred.promise;
                case APIMETHODTYPE.GET:
                    $http.get(url).
                        then(function (response) {
                            console.log(response.data)
                            deferred.resolve(response.data);
                        },
                        function (response) {
                            deferred.resolve(response.data);
                        });
                    return deferred.promise;
            }

        }
    };
});