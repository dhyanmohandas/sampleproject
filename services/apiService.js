app.factory('ApiService', function ($http, $q) {

    var data;
    return {
        getData: function () {
            if(data) {
                return $q.when(data);
            } else {
                return $http.get('https://api.football-data.org/v1/competitions/467/fixtures',{headers:{'X-Auth-Token':'761b1a0a44fe49379370e66bb4bed59f'}}).then(function(response){
                    data = response;
                    return data;
                });
            }
        }
    };
});