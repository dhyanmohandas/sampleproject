app.factory('configService', ['$http', function ($http) {

    var getBaseUrl = function () {
        var configUrl = location.origin;
        if (configUrl === 'http://localhost:8085') {
            configUrl = 'http://localhost:8080';
            /*configUrl='http://fsewallet-dev.clouddqt.capitalone.com:8080';*/
        } else {
            configUrl += ':8080';
        }
        return configUrl;
    };
    return {
        getBaseUrl: getBaseUrl
    };
}]);
