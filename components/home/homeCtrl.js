app.controller('homeController', function($state,$scope,$rootScope,$http,ApiService) {
	console.log('homeController');
	$scope.matchSelected=false;
	$scope.teamsInMatch={};
	$scope.init  = function(){
		ApiService.getData().then(function(response){
			$scope.completeFixtures=response.data.fixtures;
			$scope.predictionFixtures=$scope.completeFixtures.filter(match=>{
						return match.status=="TIMED";
					});
		});
	};
	$scope.init();

	$scope.navigateToPrediction= function(){
		$state.go('home');
	}
	$scope.navigateToLeaderBoard= function(){
		$state.go('leaderBoard');
	}

	$scope.getMatch =function(){
		$scope.matchSelected=true;
		$scope.teamsInMatch=[$scope.predictionMatch.homeTeamName,$scope.predictionMatch.awayTeamName,"Draw"];
	};
	$scope.submitButton = function(){
		var userEmail=sessionStorage.getItem("userEmail");
		console.log(userEmail);
		if(!userEmail){
			firebase.auth().onAuthStateChanged(function(user) {
			  if (user) {
			  	console.log(user);// User is signed in.
			  	console.log(user.email);
			  	userEmail=user.email;
			  } else {
			   $state.go('login');
			  }
			});
		}
		var data ={
			userId: userEmail,
			score1:$scope.score1,
			score2:$scope.score2,
			matchId: $scope.predictionMatch.date,
			submission : new Date().getTime()
		}
		var addPrediction = firebase.functions().httpsCallable('predict');
		addPrediction({
			score1: data.score1, score2: data.score2, matchID: data.matchId
		}).then(function (result) {
				// Read result of the Cloud Function.
				var pushID = result.data.predictionID;
				alert(pushID);
				console.log("PredictID - " + pushID);
		}).catch((error)=>{
			console.error(error);
			alert(error);
		});

		// $http.post('https://us-central1-goalbetting-35fbb.cloudfunctions.net/savePrediction',data)
		// 	.then(response => {
		// 		alert("Success");
		// 		console.log(data);
		// 		console.log(response)});
	};

});