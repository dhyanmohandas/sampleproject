var app = angular.module('GoalBetting', ['ui.router']);

app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'components/login/login.html',
            controller: 'loginController'
        })
        .state('home', {
            url: '/home',
            templateUrl: 'components/home/home.html',
            controller: 'homeController'
        })
        .state('leaderBoard', {
            url: '/leaderBoard',
            templateUrl: 'components/leaderBoard/leaderBoard.html',
            controller: 'leaderBoardController'
        })
        .state('registration', {
            url: '/registration',
            templateUrl: 'components/registration/registration.html',
            controller: 'registrationController'
        })

        // .state('home.launch', {
        //     url: '/home',
        //     template: '<launch-page></launch-page>'
        // })

        // .state('home.associatePayment', {
        //     url: '/associatePayment',
        //     template: '<associate-payment></associate-payment>'
        // })

        // .state('home.associatePaymentHistory', {
        //     template: '<associate-payment-history></associate-payment-history>'
        // })

    $urlRouterProvider.otherwise('/login');
}]);
